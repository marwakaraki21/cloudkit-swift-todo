//
//  ViewController.swift
//  cloudkitSwift
//
//  Created by Marwa on 19/02/2023.
//

import UIKit
import CloudKit
class ViewController: UIViewController, UITableViewDataSource {
    let tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        return tableView
    }()
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
        }
    
    private let database = CKContainer(identifier: "iCloud.marwa21").publicCloudDatabase
       
    var items = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "TODO List"
        view.addSubview(tableView)
        tableView.dataSource = self
        let control = UIRefreshControl()
        control.addTarget(self, action: #selector(fetchItems), for: .valueChanged)
        tableView.refreshControl = control
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target:self, action: #selector(didTapAdd))
        navigationController?.navigationBar.delegate = nil // Add this line
        fetchItems()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
   @objc func fetchItems(){
       tableView.refreshControl?.beginRefreshing()
        let query = CKQuery(recordType: "todoItem",predicate: NSPredicate(value: true))
        database.perform(query, inZoneWith: nil) {[weak self] records, error in
            guard let records = records, error == nil else{
                return
            }
            DispatchQueue.main.async {
                self?.items = records.compactMap({$0.value(forKey:"name") as? String})
                self?.tableView.reloadData()
                self?.tableView.refreshControl?.endRefreshing()
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell",for: indexPath)
        cell.textLabel?.text = items[indexPath.row]
        return cell
    }
    @objc func didTapAdd(){
        let alert = UIAlertController(title:"Add item", message: nil,
                                      preferredStyle: .alert)
        alert.addTextField{
            field in field.placeholder = "enter name"
        }
        alert.addAction(UIAlertAction(title:"cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title:"add",style: .default, handler:{ _ in
            if let field = alert.textFields?.first, let text = field.text, !text.isEmpty {
                self.saveItem(name: text)
            }
        }))
        present(alert, animated: true)
    }
    
    @objc func saveItem(name: String){
        let record = CKRecord(recordType: "todoItem")
        record.setValue(name, forKey: "name")
        database.save(record) { [weak self] record, error in
            if record != nil, error == nil {
                self?.fetchItems()
            }
        }
    }
}

